### PoC Blockchain PI ###

## Prerequisites
- Mac or Linux  
- Docker and docker-compose (https://www.docker.com/)  
- node 6.xx
- npm 5.xx (usually included in node package)

## Getting started

./initBollettinoProject.sh

This command will:
- start up the basic network with chaincode id "posteni" (on hlf 1.3)
- instatiate the chaincode in ./chaincode
- compile and build the web application in ./server
- run in watch mode the web application

## Clean Up

In order to clean the docker containers and temporary images:

docker rm -f -v $(docker ps -aq) 2>/dev/null;
docker rmi $(docker images -qf "dangling=true") 2>/dev/null;
docker rmi $(docker images | grep "dev-" | awk "{print $1}") 2>/dev/null; 
docker rmi $(docker images | grep "^<none>" | awk "{print $3}") 2>/dev/null;

