import * as express from "express";
import * as body from "body-parser";
import * as fs from "fs";
import * as Fabric_Client from "fabric-client";
import * as cors from "cors";

import { MyProposal } from "./myproposal";
import { Asset } from "./assets"
import { EventHubHandler } from "./eventhubhandler"
// import { EventRequest } from "./eventRequest";

class AssetController 
{
   
    public express;

    public channel;
    public peer;
    public order;

    public settings;
    private hlfclient;
    private user_from_store;
    public io;

    private config;

    // COSTRUTTORE -> fa da SINGLETON all'atto della sua istanza su Express
    constructor() 
    {
      this.SetConfig();
      this.SetClient();
      this.SetExpress();
      
      /* ACTIONs */  
      this.MountHomeRoute();
      this.SubscribeToBlockchainEvent();    
      this.GetUser();
      this.GetStub();
    }
    
    // setto le impostazioni di express
    private SetExpress() : void
    {
      /*  EXPRESS   */
      this.express = express();      
      this.express.use(body.urlencoded({ extended: false }));      
      this.express.use(body.json());      
      this.express.use(express.static(__dirname + '/public'));      
      this.express.use(express.static( __dirname + '/public/resources'));
      this.express.use(cors());
      this.init();
    }

    // event hub
    private async init(): Promise<void> {

        let store_state = await Fabric_Client.newDefaultKeyValueStore({path: this.settings.store_path});    
        this.user_from_store =  await this.setContext(store_state, this.settings.user);
        //this.SubscribeToEvent("CreateAssetEvent");
      //  this.SubscribeToEvent("UpdateAssetEvent");
        this.express.emit('ready', this.io);
    }

    // inizalizzo il client
    private SetClient() : void
    {
      /* HYPERLEDGER SDK */   
      this.hlfclient = new Fabric_Client();     
      
      // nel caso in cui il TLS sia abilitato
      if (this.config.configType == "TLSMUTUAL" )
      {
        //let clientKey = fs.readFileSync('/home/recondo77/Scrivania/workspace/PosteLab/Hyperledger/Eni/poste-asset/hlf_resources/basic-network/crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/tls/server.key');
        //let clientCert = fs.readFileSync('/home/recondo77/Scrivania/workspace/PosteLab/Hyperledger/Eni/poste-asset/hlf_resources/basic-network/crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/tls/server.crt');
        let clientKey = fs.readFileSync(this.settings.userServerKey);
        let clientCert = fs.readFileSync(this.settings.userServerCrt);
        this.hlfclient.setTlsClientCertAndKey(clientCert.toString(), clientKey.toString());

        this.channel = this.hlfclient.newChannel(this.settings.channel);
        //let peerCert = fs.readFileSync("/home/recondo77/Scrivania/workspace/PosteLab/Hyperledger/Eni/poste-asset/hlf_resources/basic-network/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt");         
        let peerCert = fs.readFileSync(this.settings.peerCaCrt);         
        //let peerOptions ={ "ssl-target-name-override": "peer0.org1.example.com", pem : peerCert.toString()};   
        let peerOptions ={ "ssl-target-name-override": this.settings.fqdnPeer, pem : peerCert.toString()};   
        //let ordererCert = fs.readFileSync("/home/recondo77/Scrivania/workspace/PosteLab/Hyperledger/Eni/poste-asset/hlf_resources/basic-network/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt");      
        let ordererCert = fs.readFileSync(this.settings.ordererCaCrt);      
        //let ordererOptions ={ "ssl-target-name-override": "orderer.example.com", pem : ordererCert.toString()};
        let ordererOptions ={ "ssl-target-name-override": this.settings.fqdnOrderer, pem : ordererCert.toString()};

        this.peer = this.hlfclient.newPeer(this.settings.peer,peerOptions); 
        this.order = this.hlfclient.newOrderer(this.settings.orderer, ordererOptions);
      }

      if (this.config.configType == "TLS" )
      {
        this.channel = this.hlfclient.newChannel(this.settings.channel);
        //let peerCert = fs.readFileSync("/home/recondo77/Scrivania/workspace/PosteLab/Hyperledger/Eni/poste-asset/hlf_resources/basic-network/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt");         
        let peerCert = fs.readFileSync(this.settings.peerCaCrt);         
        //let peerOptions ={ "ssl-target-name-override": "peer0.org1.example.com", pem : peerCert.toString()};   
        let peerOptions ={ "ssl-target-name-override": this.settings.fqdnPeer , pem : peerCert.toString()};   
        //let ordererCert = fs.readFileSync("/home/recondo77/Scrivania/workspace/PosteLab/Hyperledger/Eni/poste-asset/hlf_resources/basic-network/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt");      
        let ordererCert = fs.readFileSync(this.settings.ordererCaCrt);
        //let ordererOptions ={ "ssl-target-name-override": "orderer.example.com", pem : ordererCert.toString()};
        let ordererOptions ={ "ssl-target-name-override": this.settings.fqdnOrderer, pem : ordererCert.toString()};
        this.peer = this.hlfclient.newPeer(this.settings.peer,peerOptions); 
        this.order = this.hlfclient.newOrderer(this.settings.orderer, ordererOptions);
      }

      if (this.config.configType == "NOTLS" )
      {
        this.channel = this.hlfclient.newChannel(this.settings.channel);        
        this.peer = this.hlfclient.newPeer(this.settings.peer);
        this.order = this.hlfclient.newOrderer(this.settings.orderer);  
      }

      this.channel.addPeer(this.peer);          
      this.channel.addOrderer(this.order);

      console.log("channel: " + this.channel);
      
    }

    // prendo dal file di configurazione le impostazioni per l'SDK
    private SetConfig() : void
    {
      this.config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));
      console.log(__dirname + '/config.json');
      if (this.config.configType == "TLS")
      {
        this.settings =  JSON.parse(fs.readFileSync(__dirname + '/settingsTLS.json', 'utf8'));
      }
      else if (this.config.configType == "TLSMUTUAL")
      {
        this.settings =  JSON.parse(fs.readFileSync(__dirname + '/settingsTLS_MUTUAL.json', 'utf8'));
      }
      else if (this.config.configType == "NOTLS")
      {
        this.settings =  JSON.parse(fs.readFileSync('settingsNOTLS.json', 'utf8'));
      }

      
      console.log("#################################################################");
      console.log("");
      console.log("    _____             __ _       ");
      console.log("   / ____|           / _(_)      ");
      console.log("  | |     ___  _ __ | |_ _  __ _ ");
      console.log("  | |    / _ \| '_ \|  _| |/ _` |");
      console.log("  | |___| (_) | | | | | | | (_| |");
      console.log("   \_____\___/|_| |_|_| |_|\__, |");
      console.log("                            __/ |");
      console.log("                           |___/ ");
      console.log("");
      
      console.log("peer: " + this.settings.peer)
      console.log("orderer: " + this.settings.orderer);
      console.log("channel: " + this.settings.channel);
      console.log("store_path: " + this.settings.store_path);
      console.log("user: " + this.settings.user);
      console.log("chaincode: " + this.settings.chaincodeName);

      console.log("");    

    }

    // preparo il routing per la pagina di benvenuto
    private MountHomeRoute(): void 
    {
      const router = express.Router();
      router.get('/',function(req,res){
        res.sendFile('index.html');
        //It will find and locate index.html from View or Scripts
      });
      this.express.use('/', router);
    }


    // stub per i web client
    private GetStub() : void
    {
        const router = express.Router();
        router.get("/api/v1/stub", (req, res) => {
            res.json(JSON.parse(fs.readFileSync('dati.json', 'utf8')));
        });
        this.express.use('/', router);
        }

    // preparo il routing per una get 
    private GetUser(): void 
    {
        const router = express.Router();
        router.get("/api/v1/user", (req, res) => {

            res.json(this.settings.user);
        });

        this.express.use('/', router);
    }


    // sottoscrizione agli eventi del asset
    private SubscribeToBlockchainEvent(): void 
    {

        const router = express.Router();
        router.post("/api/v1/subscribe", async (req, res) => {
            res.json({
                message: await this.SubscribeToEvent(req.body)
            });
            //console.log(req.body);
        });
        this.express.use('/', router); 
    }

    /*****************************************************  WORKER */

    // sottoscrizione evento
    private async SubscribeToEvent(event_name:string) 
    {   /*    
        try
        {
          const eventhubhandler = new EventHubHandler(this.channel,this.peer)
          eventhubhandler.subscribeToChaincodeEvent(event_name, this.settings.chaincodeName,
            (event_payload:any) => {
                console.log("Event Received",event_name);
                //Rebroadcast the correct event name
                //CreateAssetEvent
                //PrintedAsset
                //SentAssetEvent
                //PaidAssetEvent

                let rebroadcast_event_name;
                switch(event_payload.stato) {
                    case "CREATO": rebroadcast_event_name = "CreateAssetEvent";
                    break;
                    case "STAMPATO": rebroadcast_event_name = "PrintedAssetEvent";
                    break;
                    case "POSTALIZZATO": rebroadcast_event_name = "SentAssetEvent";
                    break;
                    case "PAGATO": rebroadcast_event_name = "PaidAssetEvent";
                    break;
                }
                this.io.emit(rebroadcast_event_name, event_payload);
            })
          return "Subscribed to " + event_name
        }
        catch (e) 
        {
            console.log(e)
            return "FAIL"
        } */ 
    }

    // Query by key sul ledger
    private async Query(request: Asset.AssetRequest) : Promise<string>
    {
        let result = "EMPTY";

        try
        { 
            let query_responses = await this.SubmitQuery(request);           
	        
            if (query_responses && query_responses.length == 1) 
            {
                if (query_responses[0] instanceof Error) 
                {
                    console.error("error from query = ", query_responses[0]);
                    result = '{"RESULT","FAIL"}';
                } 
                else 
                {
                    result = JSON.parse(query_responses[0].toString());
    			          console.log("Response is ", result);
		            }
            } 
            else 
            {
		        console.log("No payloads were returned from query");
	        }
        }
        catch(ex)
        {
            console.error('Invocazione: ' + ex);
            result = '{"RESULT","FAIL"}';;        
        }
      return result;
    }

    // sottomissione di una query
    private async SubmitQuery(request: Asset.AssetRequest) : Promise<any>
    {
        //let member_user = null;
        if (this.user_from_store && this.user_from_store.isEnrolled()) 
        {
            console.log('Successfully loaded user1 from persistence');
          //  member_user = user_from_store;
        } 
        else 
        {
            throw new Error('Failed to get user1.... run registerUser.js');
        }   
        // send the query proposal to the peer
        return this.channel.queryByChaincode(request);
    }

    // Invia transazione
    private async SendTransaction (request: Asset.AssetRequest) : Promise<string> 
    { 

      let result = "EMPTY";
      try
      {
          
        let proposalResponse = await this.SendProposal(request);
        let promiseResponse = await this.PromiseAll(proposalResponse);
        this.PrintResult(promiseResponse);
        // l'ID transazione viene staccato all'atto della proposal
        result = "PASS " + proposalResponse.tx._transaction_id;
      }
      catch(ex)
      {
        console.error('Invocazione: ' + ex);
        result = "FAIL";
      }     
      return result;
    }

    // SET ENV
    // private async setContext(store_state: any, hlfclient: any, user: string ) : Promise<any> 
    private async setContext(store_state: any, user: string ) : Promise<any> 
    {
      this.hlfclient.setStateStore(store_state);
      let crypto_suite = Fabric_Client.newCryptoSuite();
      // setto l'informazione dei certificati utenti per fare la registrazione.
      let crypto_store = Fabric_Client.newCryptoKeyStore({path: this.settings.store_path});
      crypto_suite.setCryptoKeyStore(crypto_store);
      this.hlfclient.setCryptoSuite(crypto_suite);
      return this.hlfclient.getUserContext(user, true);
    }

    // SEND PROPOSAL
    private async SendProposal( request: Asset.AssetRequest ) : Promise<MyProposal>
    {   
        let myprop = new MyProposal();

        //let member_user;
        let tx_id;

        if (this.user_from_store && this.user_from_store.isEnrolled()) 
        {
          //  member_user = this.user_from_store;
        } 
        else 
        {
            throw new Error('Utente non registrato');
        }
    
        
        tx_id = this.hlfclient.newTransactionID();
        console.log("Assegnata transazione: ", tx_id._transaction_id);  

        request.args[0].idTx = tx_id._transaction_id;

        let hlfRequest = 
        {
            chaincodeId: request.chaincodeId,
            fcn: request.fcn,
            args: [JSON.stringify(request.args[0])],
            chainId: this.settings.channel,
            txId: tx_id
        };

        console.log(hlfRequest);
        // invio la proposal ai peers
        myprop.proposal = await this.channel.sendTransactionProposal(hlfRequest);        
        myprop.tx = tx_id;

        console.log("returning sendProposal")
        return myprop
    }

    // PROMISE TO ALL
    private async PromiseAll(proposalResponse: any) : Promise<any>
    {
      console.log("init promiseAll")

      let proposalResponses = proposalResponse.proposal[0];
      let proposal = proposalResponse.proposal[1];

      let isProposalGood = false;
      let txPromise;

      if (proposalResponses && proposalResponses[0].response && proposalResponses[0].response.status === 200) 
      {
        isProposalGood = true;
        console.log('Transaction proposal was good');
      } 
      else 
      {
        console.error('Transaction proposal was bad');
      }


      if (isProposalGood) 
      {
       
        // costruisco la request
        let request = 
        {
            proposalResponses: proposalResponses,
            proposal: proposal
        };

        
        let transaction_id_string = proposalResponse.tx.getTransactionID(); 
              
        var promises = [];
        let sendPromise = this.channel.sendTransaction(request);
        promises.push(sendPromise);           
        let eventhubhandler = new EventHubHandler(this.channel,this.peer)        
        txPromise = eventhubhandler.subscribeToTxEvent(transaction_id_string)      
      }
      else 
      {
        console.error('Impossibile inviare una proposal o ricevere una risposta valida. La risposta null o status non è 200. in uscita ...');
        throw new Error('Failed send Proposal or receive valid response.');
      }

      promises.push(txPromise);
      console.log("returning promiseAll")
      return Promise.all(promises);
    }

    // RESULT on console
    private PrintResult(promiseResponse : any) : void
    {
        console.log('Send transaction promise and event listener promise have completed');        
        if (promiseResponse && promiseResponse[0] && promiseResponse[0].status === 'SUCCESS')
        {
            console.log('Successfully sent transaction to the orderer.');
        } 
        else 
        {
            console.error('Failed to order the transaction. Error code: ' + promiseResponse[0].status);
        }

        if(promiseResponse && promiseResponse[1] && promiseResponse[1].event_status === 'VALID') 
        {
            console.log('Successfully committed the change to the ledger by the peer');
        }
        else
        {
            console.log('Transaction failed to be committed to the ledger due to :' + promiseResponse[1].event_status);
        }
    }

    /**************************************************************************************************************** */
}

// esporto il controller
export default new AssetController();
