export namespace Asset 
{
   

    export class Asset { 

        public idTx: string; 
        
    }

    export class AssetRequest{

        public chaincodeId: string;
        public fcn: string;
        public args: Array<Asset> = new Array();
        public chainId: string;
        public txId;

    }

}