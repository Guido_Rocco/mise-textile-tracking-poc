import app from "./app";
import * as socketIo from 'socket.io';

//const port = 3000;
const port = app.settings.listeningPort;

app.express.on('ready', function() { 
  let applisten = app.express.listen(port, (err) => {
    if (err) {
        return console.log(err);
    }
  
    return console.log(`server is listening on ${port}`);
  });

  // app.io = socketIo(applisten, {origins: 'http://localhost:4200'});
  app.io = socketIo(applisten, {origins: app.settings.IO});
  app.io.on('connect', (socket: any) => {
    console.log('client has connected');
  });
  
}); 



