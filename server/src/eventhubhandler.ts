import {Container} from 'typedi';
import {LoggerInstance} from 'winston';
import {LoggerFactory} from './utils/logger/LoggerFactory';
import { Channel } from 'fabric-client';

export class EventHubHandler {
	private logger: LoggerInstance
	private event_hub: any
	private channel:Channel

	constructor(channel: Channel, peer: any) {
		this.logger = Container.get(LoggerFactory).get('EventHubHandler')
		this.channel = channel
		this.event_hub  = channel.newChannelEventHub(peer)
	}

	//subscribe to all the events on the chaincode with name event_name
	//on event received, executes the callback function, using the payload as an input
	public async subscribeToChaincodeEvent(event_name:string, chaincode_id: string,callback: Function) {
		//registerChaincodeEvent(ccid, eventname, onEvent, onError, options)
		let blockchaininfo = await this.channel.queryInfo()
		const currentBlockHeight = blockchaininfo.height.low

		this.event_hub.registerChaincodeEvent(chaincode_id, event_name, 
		(event, block, transaction, str) => 
		{
			if (currentBlockHeight > block) { //otherwise it receives the last event of the event_name type
				this.logger.info("Successfully registered to event "+event_name)
			} else {
				this.logger.info("Received a new event: "+event_name)
				callback(JSON.parse(event.payload))
			}	
		}, 
		(error) => 
		{
			this.logger.error(error)
			throw new Error(error)
		}, 
		{})

		this.event_hub.connect(true);

	}

	//listens for the transaction with id: tx_id; disconnects on reception
	public subscribeToTxEvent(tx_id:string): Promise<any> 
	{

      // using resolve the promise so that result status may be processed
      // under the then clause rather than having the catch clause process
      // the status
      //let txPromise = new Promise((resolve, reject) => {
      let txPromise = new Promise((resolve, reject) => {

	      let handle = setTimeout(() => {
	          this.event_hub.unregisterTxEvent(tx_id);
	          this.event_hub.disconnect();
	          resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
	      }, 3000);
	          

	      //event_hub.registerTxEvent(transaction_id_string, (tx, code) => 
	      this.event_hub.registerTxEvent(tx_id, 
	      	(tx, code) => 
	        {
	            // this is the callback for transaction event status
	            // first some clean up of event listener
	            clearTimeout(handle);

	            // now let the application know what happened
	            let return_status = {event_status : code, tx_id : tx_id};
	            if (code !== 'VALID') 
	            {
	                this.logger.error('The transaction was invalid, code = ' + code);
	                resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
	            }
	            else 
	            {
	                this.logger.info('The transaction has been committed on peer ' + this.event_hub.getPeerAddr());
	                resolve(return_status);
	            }
	        },

	        (err) => 
	        {
	            //this is the callback if something goes wrong with the event registration or processing
	            reject(new Error('Errore su Event Hub :' + err));
	        },

	        {disconnect: true} //disconnect when complete
	        );

	        this.event_hub.connect();
	      });
	  
	return txPromise
	}
}

