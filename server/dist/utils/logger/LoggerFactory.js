"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston = require("winston");
const debug = require("debug");
class LoggerFactory {
    constructor(winstonOptions, morganOptions) {
        this.winstonOptions = winstonOptions;
        this.morganOptions = morganOptions;
        this.debug = debug('api:logger');
        this.debug('created loggerFactory. Winston: %O. Morgan: %O.', winstonOptions, morganOptions);
    }
    get(prefix) {
        prefix = prefix || 'default';
        this.debug(`getting logger with prefix '${prefix}'`);
        if (!LoggerFactory.loggers[prefix]) {
            const logger = new winston.Logger({
                level: 'verbose',
                transports: [
                    new winston.transports.Console({
                        timestamp: true,
                        json: false,
                        colorize: true,
                        prettyPrint: true
                    })
                ]
            });
            if (prefix !== 'default') {
                logger.filters.push((level, msg, meta) => '[${prefix}] ${msg}');
            }
            LoggerFactory.loggers[prefix] = logger;
            this.debug('created');
        }
        return LoggerFactory.loggers[prefix];
    }
}
LoggerFactory.loggers = {};
exports.LoggerFactory = LoggerFactory;
//# sourceMappingURL=LoggerFactory.js.map