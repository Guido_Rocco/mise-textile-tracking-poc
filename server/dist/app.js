"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const body = require("body-parser");
const fs = require("fs");
const Fabric_Client = require("fabric-client");
const cors = require("cors");
const myproposal_1 = require("./myproposal");
const eventhubhandler_1 = require("./eventhubhandler");
class AssetController {
    constructor() {
        this.SetConfig();
        this.SetClient();
        this.SetExpress();
        this.MountHomeRoute();
        this.SubscribeToBlockchainEvent();
        this.GetUser();
        this.GetStub();
    }
    SetExpress() {
        this.express = express();
        this.express.use(body.urlencoded({ extended: false }));
        this.express.use(body.json());
        this.express.use(express.static(__dirname + '/public'));
        this.express.use(express.static(__dirname + '/public/resources'));
        this.express.use(cors());
        this.init();
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            let store_state = yield Fabric_Client.newDefaultKeyValueStore({ path: this.settings.store_path });
            this.user_from_store = yield this.setContext(store_state, this.settings.user);
            this.express.emit('ready', this.io);
        });
    }
    SetClient() {
        this.hlfclient = new Fabric_Client();
        if (this.config.configType == "TLSMUTUAL") {
            let clientKey = fs.readFileSync(this.settings.userServerKey);
            let clientCert = fs.readFileSync(this.settings.userServerCrt);
            this.hlfclient.setTlsClientCertAndKey(clientCert.toString(), clientKey.toString());
            this.channel = this.hlfclient.newChannel(this.settings.channel);
            let peerCert = fs.readFileSync(this.settings.peerCaCrt);
            let peerOptions = { "ssl-target-name-override": this.settings.fqdnPeer, pem: peerCert.toString() };
            let ordererCert = fs.readFileSync(this.settings.ordererCaCrt);
            let ordererOptions = { "ssl-target-name-override": this.settings.fqdnOrderer, pem: ordererCert.toString() };
            this.peer = this.hlfclient.newPeer(this.settings.peer, peerOptions);
            this.order = this.hlfclient.newOrderer(this.settings.orderer, ordererOptions);
        }
        if (this.config.configType == "TLS") {
            this.channel = this.hlfclient.newChannel(this.settings.channel);
            let peerCert = fs.readFileSync(this.settings.peerCaCrt);
            let peerOptions = { "ssl-target-name-override": this.settings.fqdnPeer, pem: peerCert.toString() };
            let ordererCert = fs.readFileSync(this.settings.ordererCaCrt);
            let ordererOptions = { "ssl-target-name-override": this.settings.fqdnOrderer, pem: ordererCert.toString() };
            this.peer = this.hlfclient.newPeer(this.settings.peer, peerOptions);
            this.order = this.hlfclient.newOrderer(this.settings.orderer, ordererOptions);
        }
        if (this.config.configType == "NOTLS") {
            this.channel = this.hlfclient.newChannel(this.settings.channel);
            this.peer = this.hlfclient.newPeer(this.settings.peer);
            this.order = this.hlfclient.newOrderer(this.settings.orderer);
        }
        this.channel.addPeer(this.peer);
        this.channel.addOrderer(this.order);
        console.log("channel: " + this.channel);
    }
    SetConfig() {
        this.config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));
        console.log(__dirname + '/config.json');
        if (this.config.configType == "TLS") {
            this.settings = JSON.parse(fs.readFileSync(__dirname + '/settingsTLS.json', 'utf8'));
        }
        else if (this.config.configType == "TLSMUTUAL") {
            this.settings = JSON.parse(fs.readFileSync(__dirname + '/settingsTLS_MUTUAL.json', 'utf8'));
        }
        else if (this.config.configType == "NOTLS") {
            this.settings = JSON.parse(fs.readFileSync('settingsNOTLS.json', 'utf8'));
        }
        console.log("#################################################################");
        console.log("");
        console.log("    _____             __ _       ");
        console.log("   / ____|           / _(_)      ");
        console.log("  | |     ___  _ __ | |_ _  __ _ ");
        console.log("  | |    / _ \| '_ \|  _| |/ _` |");
        console.log("  | |___| (_) | | | | | | | (_| |");
        console.log("   \_____\___/|_| |_|_| |_|\__, |");
        console.log("                            __/ |");
        console.log("                           |___/ ");
        console.log("");
        console.log("peer: " + this.settings.peer);
        console.log("orderer: " + this.settings.orderer);
        console.log("channel: " + this.settings.channel);
        console.log("store_path: " + this.settings.store_path);
        console.log("user: " + this.settings.user);
        console.log("chaincode: " + this.settings.chaincodeName);
        console.log("");
    }
    MountHomeRoute() {
        const router = express.Router();
        router.get('/', function (req, res) {
            res.sendFile('index.html');
        });
        this.express.use('/', router);
    }
    GetStub() {
        const router = express.Router();
        router.get("/api/v1/stub", (req, res) => {
            res.json(JSON.parse(fs.readFileSync('dati.json', 'utf8')));
        });
        this.express.use('/', router);
    }
    GetUser() {
        const router = express.Router();
        router.get("/api/v1/user", (req, res) => {
            res.json(this.settings.user);
        });
        this.express.use('/', router);
    }
    SubscribeToBlockchainEvent() {
        const router = express.Router();
        router.post("/api/v1/subscribe", (req, res) => __awaiter(this, void 0, void 0, function* () {
            res.json({
                message: yield this.SubscribeToEvent(req.body)
            });
        }));
        this.express.use('/', router);
    }
    SubscribeToEvent(event_name) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    Query(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = "EMPTY";
            try {
                let query_responses = yield this.SubmitQuery(request);
                if (query_responses && query_responses.length == 1) {
                    if (query_responses[0] instanceof Error) {
                        console.error("error from query = ", query_responses[0]);
                        result = '{"RESULT","FAIL"}';
                    }
                    else {
                        result = JSON.parse(query_responses[0].toString());
                        console.log("Response is ", result);
                    }
                }
                else {
                    console.log("No payloads were returned from query");
                }
            }
            catch (ex) {
                console.error('Invocazione: ' + ex);
                result = '{"RESULT","FAIL"}';
                ;
            }
            return result;
        });
    }
    SubmitQuery(request) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.user_from_store && this.user_from_store.isEnrolled()) {
                console.log('Successfully loaded user1 from persistence');
            }
            else {
                throw new Error('Failed to get user1.... run registerUser.js');
            }
            return this.channel.queryByChaincode(request);
        });
    }
    SendTransaction(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = "EMPTY";
            try {
                let proposalResponse = yield this.SendProposal(request);
                let promiseResponse = yield this.PromiseAll(proposalResponse);
                this.PrintResult(promiseResponse);
                result = "PASS " + proposalResponse.tx._transaction_id;
            }
            catch (ex) {
                console.error('Invocazione: ' + ex);
                result = "FAIL";
            }
            return result;
        });
    }
    setContext(store_state, user) {
        return __awaiter(this, void 0, void 0, function* () {
            this.hlfclient.setStateStore(store_state);
            let crypto_suite = Fabric_Client.newCryptoSuite();
            let crypto_store = Fabric_Client.newCryptoKeyStore({ path: this.settings.store_path });
            crypto_suite.setCryptoKeyStore(crypto_store);
            this.hlfclient.setCryptoSuite(crypto_suite);
            return this.hlfclient.getUserContext(user, true);
        });
    }
    SendProposal(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let myprop = new myproposal_1.MyProposal();
            let tx_id;
            if (this.user_from_store && this.user_from_store.isEnrolled()) {
            }
            else {
                throw new Error('Utente non registrato');
            }
            tx_id = this.hlfclient.newTransactionID();
            console.log("Assegnata transazione: ", tx_id._transaction_id);
            request.args[0].idTx = tx_id._transaction_id;
            let hlfRequest = {
                chaincodeId: request.chaincodeId,
                fcn: request.fcn,
                args: [JSON.stringify(request.args[0])],
                chainId: this.settings.channel,
                txId: tx_id
            };
            console.log(hlfRequest);
            myprop.proposal = yield this.channel.sendTransactionProposal(hlfRequest);
            myprop.tx = tx_id;
            console.log("returning sendProposal");
            return myprop;
        });
    }
    PromiseAll(proposalResponse) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("init promiseAll");
            let proposalResponses = proposalResponse.proposal[0];
            let proposal = proposalResponse.proposal[1];
            let isProposalGood = false;
            let txPromise;
            if (proposalResponses && proposalResponses[0].response && proposalResponses[0].response.status === 200) {
                isProposalGood = true;
                console.log('Transaction proposal was good');
            }
            else {
                console.error('Transaction proposal was bad');
            }
            if (isProposalGood) {
                let request = {
                    proposalResponses: proposalResponses,
                    proposal: proposal
                };
                let transaction_id_string = proposalResponse.tx.getTransactionID();
                var promises = [];
                let sendPromise = this.channel.sendTransaction(request);
                promises.push(sendPromise);
                let eventhubhandler = new eventhubhandler_1.EventHubHandler(this.channel, this.peer);
                txPromise = eventhubhandler.subscribeToTxEvent(transaction_id_string);
            }
            else {
                console.error('Impossibile inviare una proposal o ricevere una risposta valida. La risposta null o status non è 200. in uscita ...');
                throw new Error('Failed send Proposal or receive valid response.');
            }
            promises.push(txPromise);
            console.log("returning promiseAll");
            return Promise.all(promises);
        });
    }
    PrintResult(promiseResponse) {
        console.log('Send transaction promise and event listener promise have completed');
        if (promiseResponse && promiseResponse[0] && promiseResponse[0].status === 'SUCCESS') {
            console.log('Successfully sent transaction to the orderer.');
        }
        else {
            console.error('Failed to order the transaction. Error code: ' + promiseResponse[0].status);
        }
        if (promiseResponse && promiseResponse[1] && promiseResponse[1].event_status === 'VALID') {
            console.log('Successfully committed the change to the ledger by the peer');
        }
        else {
            console.log('Transaction failed to be committed to the ledger due to :' + promiseResponse[1].event_status);
        }
    }
}
exports.default = new AssetController();
//# sourceMappingURL=app.js.map