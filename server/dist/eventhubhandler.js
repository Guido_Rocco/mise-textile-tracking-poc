"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const LoggerFactory_1 = require("./utils/logger/LoggerFactory");
class EventHubHandler {
    constructor(channel, peer) {
        this.logger = typedi_1.Container.get(LoggerFactory_1.LoggerFactory).get('EventHubHandler');
        this.channel = channel;
        this.event_hub = channel.newChannelEventHub(peer);
    }
    subscribeToChaincodeEvent(event_name, chaincode_id, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let blockchaininfo = yield this.channel.queryInfo();
            const currentBlockHeight = blockchaininfo.height.low;
            this.event_hub.registerChaincodeEvent(chaincode_id, event_name, (event, block, transaction, str) => {
                if (currentBlockHeight > block) {
                    this.logger.info("Successfully registered to event " + event_name);
                }
                else {
                    this.logger.info("Received a new event: " + event_name);
                    callback(JSON.parse(event.payload));
                }
            }, (error) => {
                this.logger.error(error);
                throw new Error(error);
            }, {});
            this.event_hub.connect(true);
        });
    }
    subscribeToTxEvent(tx_id) {
        let txPromise = new Promise((resolve, reject) => {
            let handle = setTimeout(() => {
                this.event_hub.unregisterTxEvent(tx_id);
                this.event_hub.disconnect();
                resolve({ event_status: 'TIMEOUT' });
            }, 3000);
            this.event_hub.registerTxEvent(tx_id, (tx, code) => {
                clearTimeout(handle);
                let return_status = { event_status: code, tx_id: tx_id };
                if (code !== 'VALID') {
                    this.logger.error('The transaction was invalid, code = ' + code);
                    resolve(return_status);
                }
                else {
                    this.logger.info('The transaction has been committed on peer ' + this.event_hub.getPeerAddr());
                    resolve(return_status);
                }
            }, (err) => {
                reject(new Error('Errore su Event Hub :' + err));
            }, { disconnect: true });
            this.event_hub.connect();
        });
        return txPromise;
    }
}
exports.EventHubHandler = EventHubHandler;
//# sourceMappingURL=eventhubhandler.js.map