"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
const socketIo = require("socket.io");
const port = app_1.default.settings.listeningPort;
app_1.default.express.on('ready', function () {
    let applisten = app_1.default.express.listen(port, (err) => {
        if (err) {
            return console.log(err);
        }
        return console.log(`server is listening on ${port}`);
    });
    app_1.default.io = socketIo(applisten, { origins: app_1.default.settings.IO });
    app_1.default.io.on('connect', (socket) => {
        console.log('client has connected');
    });
});
//# sourceMappingURL=server.js.map