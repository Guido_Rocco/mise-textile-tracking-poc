"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Bollettino;
(function (Bollettino) {
    class BolloVirtuale {
        constructor(dataOperazione, numeroSportello, numeroProgressivo, frazionario, descrizioneUfficio, siglaPagamento) {
            this.dataOperazione = dataOperazione;
            this.descrizioneUfficio = descrizioneUfficio;
            this.frazionario = frazionario;
            this.numeroProgressivo = numeroProgressivo;
            this.numeroSportello = numeroSportello;
            this.siglaPagamento = siglaPagamento;
        }
    }
    Bollettino.BolloVirtuale = BolloVirtuale;
    class BollettinoBase {
        constructor(quintoCampo, importo, conto, formatCodeLine) {
            this.quintoCampo = quintoCampo;
            this.conto = conto;
            this.formatCodeLine = formatCodeLine;
            this.importo = importo;
        }
    }
    Bollettino.BollettinoBase = BollettinoBase;
    class BollettinoPremarcato extends BollettinoBase {
        constructor(commissione, bolloVirtuale, contoCorrenteDestinatario, eseguitoDa, indirizzo, cap, citta, provincia, importoTotale, tipoDocumento, codeLine, conto, formatCodeLine, quintoCampo, importo) {
            super(quintoCampo, importo, conto, formatCodeLine);
            this.bolloVirtuale = null;
            this.cap = cap;
            this.citta = citta;
            this.codeLine = codeLine;
            this.commissione = commissione;
            this.conto = conto;
            this.contoCorrenteDestinatario = contoCorrenteDestinatario;
            this.eseguitoDa = eseguitoDa;
            this.formatCodeLine = formatCodeLine;
            this.importo = this.importo;
            this.importoTotale = importoTotale;
            this.tipoDocumento = tipoDocumento;
            this.indirizzo = indirizzo;
        }
    }
    Bollettino.BollettinoPremarcato = BollettinoPremarcato;
    class BollettinoRequest {
        constructor() {
            this.args = new Array();
        }
    }
    Bollettino.BollettinoRequest = BollettinoRequest;
})(Bollettino = exports.Bollettino || (exports.Bollettino = {}));
//# sourceMappingURL=bollettinoasset.js.map