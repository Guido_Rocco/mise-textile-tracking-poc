#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error
set -e

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1
starttime=$(date +%s)
CC_SRC_PATH=/opt/gopath/src/github.com/

# clean the keystore
rm -rf ./hlf_resources/hfc-key-store

printf "> removed old keystore"

# launch network; create channel and join peer to channel
cd ./hlf_resources/basic-network
./start_TLS_MUTUAL.sh

# Now launch the CLI container in order to install, instantiate chaincode
# and prime the ledger with our 10 cars
docker-compose -f ./docker-compose_TLS_MUTUAL.yml up -d cli

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" cli peer chaincode install -n posteni -v 1.0 -p "$CC_SRC_PATH" -l "node"
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" cli peer chaincode instantiate -o orderer.example.com:7050 -C mychannel -n posteni -l "node" -v 1.0 -c '{"Args":[""]}' -P "OR ('Org1MSP.member','Org2MSP.member')" --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
sleep 10
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C mychannel -n posteni -c '{"args":["{\"quintoCampo\":\"q1\",\"conto\":\"contox\",\"formatCodeLine\":\"xxxxx-yyyyy\",\"importo\":122,\"bolloVirtuale\":null,\"cap\":\"81100\",\"citta\":\"Caserta\",\"codeLine\":\"1234567890123456m93\",\"commissione\":1.5,\"contoCorrenteDestinatario\":\"conto1\",\"eseguitoDa\":\"Massimo Rossignuolo\",\"importoTotale\":123.5,\"tipoDocumento\":\"896\",\"indirizzo\":\"via SS nome di Maria\", \"codiceFiscale\": \"CHNLEI88H06F205M\"}"],"function":"initLedger"}' --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem


printf "> finished hlf setup"

cd ..
npm install
node enrollAdmin.js
node registerUser.js 

printf "> created admin and user resources"

cd ../server 
npm run build 
npm run watch