'use strict';

const shim = require('fabric-shim');
const util = require('util');

let Chaincode = class 
{

    /* INIT */
   async Init(stub) 
    {
        console.info('=========== Instanziato il chaincode bollettino ===========');
        return shim.success();
   }

    /* INVOKE */
    async Invoke(stub)
    {
        let ret = stub.getFunctionAndParameters();
        console.info(ret);

        console.log("DESCRITTORE STUB : " + stub);

        let method = this[ret.fcn];
        if (!method) 
        {''
          console.error('Il nome della funzione:' + ret.fcn + ' non è presente');
          throw new Error('Il nome della funzione: ' + ret.fcn + ' non è presente');
        }
        try 
        {
          let payload = await method(stub, ret.params);
          return shim.success(payload);
        } 
        catch (err) 
        {
          console.log("ERRORE -> " + err);
          return shim.error(err);
        }
    }
};

shim.start(new Chaincode());
